package lab4;

public class Rectangle {
	
	int sideA;
	int sideB;
	
	public Rectangle(int a, int b){
		sideA=a;
		sideB=b;
		System.out.println("creating rectangle");
	}
	
	public int area(){
		int result=sideA*sideB;   // direk return = sideA*sideB yazabilirdik
		return result;
	}
	public int perimeter(){
		return 2*(sideA+sideB);
	}
}
