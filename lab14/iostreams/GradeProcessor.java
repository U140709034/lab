package iostreams;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class GradeProcessor {

	public static void main(String[] args) throws IOException {
		GradeProcessor gp = new GradeProcessor();
		String fileName = "lab14/HWGrades.txt";
		try{
				gp.processFile("lab14/HWGrades.txt");
		} catch (FileNotFoundException e){
			System.out.println(fileName + " does not exist");
		}

	}
	
	private void processFile(String inputFileName) throws IOException{
		File file = new File(inputFileName);
		
		//FileInputStream reader = new FileInputStream(inputFileName);
		BufferedReader reader = new BufferedReader(new FileReader(file));
		FileWriter writer = new FileWriter("out.txt");
		
		
		String line = reader.readLine();
		int count= 0;
		
		while(line != null){
			count++;
			if(count > 1){
				String[] items = line.split("\t");
				double total = 0;
				for(int i = 1 ; i < items.length ; i++){
					total += Integer.parseInt(items[i]);
				}
				double avg = (double) total / (items.length - 1);
				System.out.print(items[0] + "\t" + avg + "\n");
				writer.write(items[0] + "\t"+ avg + "\n");
			}
			
			
			line = reader.readLine();
		}
		
		System.out.println("Number of chars: " + count);
		System.out.println(file.length());
		
		reader.close();
		writer.close();
	}

}