package demo;

import stack.Stack;
import stack.StackImpl;

public class StackDemo {
	
	public static void main(String[] args){
		Stack<Integer> stack = new StackImpl<>();
		stack.push(5);
		stack.push(3);
		stack.push(5);
		stack.push(9);
		
		Stack<Integer> stack2 = new StackImpl<>();
		stack.push(7);
		stack.push(5);
		stack.push(4);
		
		stack.addAll(stack2);
		
		
		System.out.println(stack.toList());
		
		Stack<Integer> st = stack;
		
		int total= 0;
		while(!st.empty()){
			System.out.println((Integer)st.pop());       
		}
		System.out.println(total);
		
		
		
		
	}
}
